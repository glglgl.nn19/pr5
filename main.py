def convert(number, unit1, unit2):
    SI = {'mm': 0.001, 'sm': 0.01, 'dm': 0.1, 'm': 1.0, 'km': 1000}
    with open('test.txt', 'a') as file:
        file.write(str(number) + str(unit1) + " = " + str(number * SI[unit1]/SI[unit2]) + str(unit2) + "\n")
    return (number * (SI[unit1] / SI[unit2]))
